# GludProject

Proyecto realizado como final del curso Gnubies de la Universidad Disrital Francisco Jose de Caldas, con el fin de mantener y perdurar lo que fue dicho espacio para profesionales y estudiantes durante su estadia en la educacion superior, conmemorando los grandes concocimientos que se grabaron no unicamente en las paredes de dicho lugar sino en las mentes y corazones de los que tuvieron el privilegio de pertenecer a dicho espacio de sabiduria y conocimiento.

<img src="https://imgur.com/umnCixt.jpg" alt="Paso 2" />
<img src="https://imgur.com/4q9a4F8.jpg" alt="Paso 2" />


## Authors and acknowledgment

Carlos S Mora Hoyos

## Description

Modelamiento de la Sala Grupo GNU/Linux Universidad Distrital, Agosto 2022. Realizado con la herramienta Blender.

## Download
Para la descargar unicamente debes descargar el archivo adjunto al repositoria, el cual tiene una extension .fbx y seguir los pasos a continuacion.

> <center><img src="https://imgur.com/2AXHJwN.jpg" alt="Paso 2" width="50%"/></center>

<img src="https://imgur.com/OVHuWdb.jpg" alt="Paso 2" width="50%"/>

Recuerda que puedes varias entre diferentes tipos de vistas, como se muestras en las siguientes imagenes, en la parte superior derecha puedes cambiar entre la vista de modelado(primera imagen) y vista con las texturas(segunda imagen), finalmente y siguiendo la lines de opciones la ultima opcion te dejara mostrar el render listo para exportar te recomendamos tener cuidado pues dicha vista consume bastante recursos.

<img src="https://imgur.com/B2f9tcV.jpg" alt="Paso 2" width="50%"/>

<img src="https://imgur.com/JuJq1rO.jpg" alt="Paso 2" width="50%"/>



## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
